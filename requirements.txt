virtualenv >= 15.0
flask >= 0.12.2
flask_cors >= 3.0.3
easysnmp >= 0.2.5
