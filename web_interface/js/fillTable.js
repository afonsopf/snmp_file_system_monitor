var arr_indexs;
var arr_parts;
var arr_size;
var arr_free;

function getIndexs(){
    
    $.ajax({
        type: "GET",
        url: "http://localhost:5000/indexs",
        success: function(data){ arr_indexs = data; },
        error: function(error){console.log(error)}
        }
    );
}

function getDisks(){
    $.ajax({
        type: "GET",
        url: "http://localhost:5000/disks",
        success: function(data){ arr_parts = data; },
        error: function(error){console.log(error)}
        }
    );
}


function getSize(){
    $.ajax({
        type: "GET",
        url: "http://localhost:5000/size",
        success: function(data){ arr_size = data; },
        error: function(error){console.log(error)}
        }
    );
}


function getFree(row,index){
    
    $.ajax({
        type: "GET",
        url: "http://localhost:5000/free/"+index,
        success: function(data){
            insertFree(row,data); 
            if(data<15){ 
                setTimeout(function(){ getFree(row,index); }, 15000);
            }
            else if(data<30){ 
                setTimeout(function(){ getFree(row,index); }, 30000);
            } 
            else{ 
                setTimeout(function(){ getFree(row,index); }, 60000);
            } 
            
        },
        error: function(error){console.log(error)}
        }
    );
}


function fillTable(){
    var table = document.getElementById('dataTable');
    setTimeout(function(){
    var i = 0;
    var row, cell1, cell2, cell3;
    while(i < arr_parts.length){
        row = table.insertRow(i+1);
        cell1 = row.insertCell(0);
        cell2 = row.insertCell(1);
        cell3 = row.insertCell(2);
        cell1.innerHTML = arr_parts[i];
        cell2.innerHTML = Math.round(arr_size[i]/1000000000 * 10) / 10;
        getFree(i,arr_indexs[i]);
        i++;
    }
    }, 250);
    
}


function insertFree(row,free_space){
    console.log("bla");
    var table = document.getElementById('dataTable');
    free_space = Math.round(free_space * 10) / 10;
    row = table.rows[row+1];
    cell = row.cells[2];
    cell.innerHTML = free_space;
    if(free_space < 15){
        row.style.color = "red";
    } else if(free_space < 30){
        row.style.color = "orange";
    } else{
        row.style.color = "MidnightBlue";
    }
}


$(document).ready(function(){
    getIndexs();
    getDisks();
    getSize();
    fillTable();
    
    $("#submit-button").click(function(e){
        e.preventDefault();
        alert(document.getElementById("address-box").value);
        $.ajax({
            type: "POST",
            url: "http://localhost:5000/address",
            data: {address: document.getElementById("address-box").value},
            success: function(){ alert("Address changed"); },
            error: function(error){console.log(error);}
        });
    });
    
});




