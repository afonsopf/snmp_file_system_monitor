from flask import Flask
from flask import jsonify
from flask_cors import CORS
from flask import request
from easysnmp import Session
from multiprocessing import Process
import time
import threading
import json

agent_address = "127.0.0.1"
agent_port = 161
session = Session(hostname=agent_address, remote_port=agent_port, community='public', version=2)


partition_index = []
partition_list = []
allocation_units = []
total_space = []



app = Flask(__name__)
CORS(app)

def get_data():
    global partition_index, partition_list, allocation_units, total_space
    partition_index = session.walk("hrFSStorageIndex")
    for p in partition_index:
        partition_list += [session.get("hrStorageDescr.%d" % int(p.value))]
        allocation_units += [session.get("hrStorageAllocationUnits.%d" % int(p.value))]
        total_space += [session.get("hrStorageSize.%d" % int(p.value))]

    
def get_used_space(indexToGet):
    i=0
    while i < len(partition_index):
        if int(partition_index[i].value) == indexToGet:
            break
        i += 1
    if i == len(partition_index):
        raise Exception("Partição não encontrada")
    return (i, session.get("hrStorageUsed.%d" % int(partition_index[i].value)).value)


get_data()


@app.route('/indexs', methods=['GET'])
def get_indexs_list():

    return_list = []
    i = 0
    for p in partition_index:
        return_list += [p.value]
    return jsonify(return_list)

@app.route('/disks', methods=['GET'])
def get_disk_list():
    return_list = []
    i = 0
    for p in partition_list:
        return_list += [p.value]
    return jsonify(return_list)


@app.route('/size', methods=['GET'])
def get_size_list():
    
    return_list = []
    i = 0
    for p in total_space:
        return_list += [int(p.value) * int(allocation_units[i].value)]
        i += 1
    
    return jsonify(return_list)


@app.route('/free/<int:index>', methods=['GET'])
def get_free_space(index):
    i,used_space = get_used_space(index)
    if int(total_space[i].value) != 0:
        return_value = (1- int(used_space) / int(total_space[i].value)) * 100
    else:
        return_value = 0
    return str(return_value)

@app.route('/address',methods=['POST'])
def set_address():
    global agent_address
    global agent_port
    global session
    request_address = request.form.getlist("address")[0]
    agent_address = request_address.split(":")[0]
    agent_port = request_address.split(":")[1]
    session = Session(hostname=agent_address, remote_port=agent_port, community='public', version=2)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

if __name__ == '__main__':
    app.run(debug=True)
